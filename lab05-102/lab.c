#include <stdio.h>

void fill_arr(int *arr, int size);
void print_arr(int *arr, int size);

void print_mat(int *m, int h, int w){
    int (*m2d)[w] = (int(*)[])m;
    for(int i =0 ; i < h; ++i) {
        for(int j = 0; j < w; ++j) {
            printf("%d ", m2d[i][j]);
            //printf("%d ", m[i*w + j]);
        }
        putchar('\n');
    }
}

int main()
{
    int h = 2;
    int w = 3;
    int mat[] = {1,2,3,4,5,6};

    print_mat(mat, h, w);

    return 0;

    // int arr[5] = {1,2,3,4,5};
    // for(int i = 0; i < 5; ++i) {
    //     printf("%d ", arr[i]);
    // }
    // putchar('\n');
    // putchar('\n');

    // int ((mat[2])[3]) = { {1,2,3}, {4,5,6} };
    // mat[0][3] = 10;
    // for(int i = 0; i < 2; ++i) {
    //     for(int j = 0; j < 3; ++j) {
    //         printf("%d ", ( (mat[i]) [j]) );
    //     }
    //     putchar('\n');
    // }
    // int *mat1d = (int*)mat;
    // for(int i =0 ; i < 6; ++i) {
    //     printf("%d ", mat1d[i]);
    // }
    // putchar('\n');

    int size;
    if(scanf("%d", &size) != 1) {
        return 100;
    }
    int vaaar[size];
    printf("sizeof(int): %lu\n", sizeof(size));
    printf("sizeof(char): %lu\n", sizeof(char));
    printf("sizeof(vaaar): %lu\n", sizeof(vaaar));
    printf("len(vaaar): %lu\n", sizeof(vaaar)/sizeof(int));




    fill_arr(vaaar, size);
    print_arr(vaaar, size);

    return 0;
}

void fill_arr(int *arr, int size)
{
    printf("sizeof(vaaar)(inside fill_arr()): %lu\n", sizeof(arr));
    for(int i = 0; i < size; ++i) {
        scanf("%d", &arr[i]);
    }
}

void print_arr(int *arr, int size)
{
    for(int i = 0; i < size; ++i) {
        printf("%d ", arr[i]);
    }
    putchar('\n');
}
