#include <stdio.h>

int mystrlen(char *str);
int mystrcmp(char *str1, char *str2);

int main()
{
    char *str = "everybody likes PRG";
    char str2[] = "Everybody likes PRGA";



    printf("len(%s): %d\n", str, mystrlen(str));
    printf("len(%s): %d\n", str2, mystrlen(str2));
    printf("cmp(%s, %s): %d\n", str, str2, mystrcmp(str, str2));

    return 0;
}

int mystrcmp(char *str1, char *str2)
{
    int diff_i;
    for(diff_i = 0;
        str1[diff_i] == str2[diff_i]
        && str1[diff_i] != '\0'
        && str2[diff_i] != '\0';
        ++diff_i);
    
    if(str1[diff_i] < str2[diff_i]) {
        return -1;
    } else if (str1[diff_i] > str2[diff_i]) {
        return 1;
    } else {
        return 0;
    }
}

int mystrlen(char *str)
{
    int size;
    for(size = 0; str[size] != '\0'; ++size);
    return size;
}