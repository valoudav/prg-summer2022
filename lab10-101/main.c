#include <stdio.h>
#include <stdbool.h>

#include <unistd.h>
#include <pthread.h>

void* thread1(void*);
void* thread2(void*);

void* input_thrd(void*);
void* output_thrd(void*);
void* counter_thrd(void*);

char *names[] = {"Input", "Output", "Counter"};
void* (*thread_funs[])(void*) = {input_thrd, output_thrd, counter_thrd};
enum {INPUT, OUTPUT, COUNTER, N_THREADS};
pthread_t threads[N_THREADS];

pthread_mutex_t counter_mtx;
pthread_cond_t counter_cond;
int counter;
bool quit = false;

int main(void)
{
   int r = 0;
   r += pthread_mutex_init(&counter_mtx, NULL);
   r += pthread_cond_init(&counter_cond, NULL);
   if(r != 0) {
      fprintf(stderr, "ERROR: Failed to init mutex or cond\n");
      return 100;
   }
   counter = 0;

   for(int i = 0; i < N_THREADS; ++i) {
      int r = pthread_create(&threads[i], NULL, thread_funs[i], NULL);
      printf("Creating thread '%s': %s\n", names[i], (r == 0 ? "OK" : "FAIL"));
   }

   getchar();
   quit = true;

   for(int i = 0; i < N_THREADS; ++i) {
      int r = pthread_join(threads[i], NULL);
      printf("Joined thread '%s': %s\n", names[i], (r == 0 ? "OK" : "FAIL"));
   }

   return 0;
}

void* input_thrd(void* arg)
{
   //TODO
   return 0;
}

void* output_thrd(void* arg)
{
   bool q = false;
   while (!q) {
      pthread_mutex_lock(&counter_mtx);
      pthread_cond_wait(&counter_cond, &counter_mtx);

      printf("\rCounter %10i", counter);
      fflush(stdout);

      pthread_mutex_unlock(&counter_mtx);

      q = quit;
   }
   putchar('\n');
   return 0;
}

void* counter_thrd(void* arg)
{
   bool q = false;
   while (!q) {
      usleep(100 * 1000);
      pthread_mutex_lock(&counter_mtx);
      counter += 1;
      pthread_cond_signal(&counter_cond);
      pthread_mutex_unlock(&counter_mtx);
      q = quit;
   }
   pthread_mutex_lock(&counter_mtx);
   pthread_cond_signal(&counter_cond);
   pthread_mutex_unlock(&counter_mtx);
   return 0;
}

void* thread1(void *v)
{
   bool q = false;
   // while (!q) {
   //    usleep(100 * 1000);
   //    counter += 1;
   //    q = quit;
   // }
   for(int i = 0; i < 10; ++i) {
      usleep(1 * 1000);
      pthread_mutex_lock(&counter_mtx);
      counter += 1;
      pthread_cond_signal(&counter_cond);
      pthread_mutex_unlock(&counter_mtx);
   }
   return 0;
}

void* thread2(void *v) 
{
   bool q = false;
   int n_print = 0;
   while (!q) {
      pthread_mutex_lock(&counter_mtx);
      pthread_cond_wait(&counter_cond, &counter_mtx);

      printf("\rCounter %10i", counter);
      pthread_mutex_unlock(&counter_mtx);

      n_print += 1;
      fflush(stdout);
      q = quit;
   }
   putchar('\n');
   printf("n_print: %d\n", n_print);
   return 0;
}
