#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct student {
	char name [9];
	int points;
} student_t;

typedef struct stud_node {
	student_t stud;
	struct stud_node *next;
} stud_node_t;

typedef struct queue {
	stud_node_t *first;
	stud_node_t *last;
} queue_t;

void init_queue(queue_t *queue)
{
	queue->first = queue->last = NULL;
}

bool queue_empty(queue_t *queue)
{
	return queue->first == NULL;
}

bool push_student(queue_t *queue, const student_t *stud)
{
	stud_node_t *new = (stud_node_t*)malloc(sizeof(stud_node_t));
	if(new == NULL) {
		return false;
	}
	new->stud = *stud;
	new->next = NULL;

	if(queue_empty(queue)) {
		queue->first = queue->last = new;
	} else {
		queue->last->next = new;
		queue->last = new;
	}

	return true;
}

bool push_student_priority(queue_t *queue, const student_t *stud)
{
	stud_node_t *new = (stud_node_t*)malloc(sizeof(stud_node_t));
	if(new == NULL) {
		return false;
	}
	new->stud = *stud;
	new->next = NULL;

	if(queue_empty(queue)) {
		queue->first = queue->last = new;
	} else if (queue->first->stud.points < new->stud.points) {
		new->next = queue->first;
		queue->first = new;
	} else {
		stud_node_t *insert_point = queue->first;
		while(insert_point->next != NULL && insert_point->next->stud.points >= new->stud.points) {
			insert_point = insert_point->next;
		}
		new->next = insert_point->next;
		insert_point->next = new;
	}

	return true;
}

student_t pop_student(queue_t *queue)
{
	if(queue_empty(queue)) {
		return (student_t){"", 0};
	}

	student_t ret = queue->first->stud;

	stud_node_t *tmp = queue->first;
	if(queue->first->next == NULL) {
		init_queue(queue);
	} else {
		queue->first = queue->first->next;
	}
	free(tmp);

	return ret;
}

void delete_queue(queue_t *queue)
{
	while(!queue_empty(queue)) {
		pop_student(queue);
	}
}

int main(void)
{
	student_t students[] = {
		{"valoudav", 10},
		{"valouda1", 0},
		{"valouda2", 5},
		{"valouda3", 15},
		{"valouda4", 7}
	};

	queue_t queue;
	init_queue(&queue);

	for(int i = 0; i < 5; ++i) {
		push_student(&queue, &students[i]); /* FIFO queue */
		// push_student_priority(&queue, &students[i]); /* priority queue */
	}

	while(!queue_empty(&queue)) {
		student_t s = pop_student(&queue);
		printf("%s, %d\n", s.name, s.points);
	}

	delete_queue(&queue);

	return 0;
}
