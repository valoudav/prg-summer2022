#include<stdio.h>

#define ARR_SIZE 5

int main(int argc, char const *argv[])
{
    int arr[ARR_SIZE];

    for (int i = 0; i < ARR_SIZE; i++)
    {
        arr[i] = 1 << i;
    }

    // nějaký komplexní výpočet na polem,
    // jinak moc nedává smysl používat pole
    // a můžeme provádět výpočty rovnou

    for (int i = 0; i < ARR_SIZE; i++)
    {
        printf("%d ", arr[i]);
    }
    putchar('\n');   

    return 0;
}
