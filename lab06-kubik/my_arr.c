#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

#define TYPE short

// ERRORS by task
#define ERROR_INPUT 100
#define ERROR_NEGATIVE 101

// memory fail error, in sequence to previous
#define ERROR_MEM_FAIL 102

// defining my structure
struct My_Arr
{
    TYPE* arr;
    int size;
};

// defining my new data type
typedef struct My_Arr MyArr;

// Ask user about arr size and allocate it
// - arr: pointer to data
// - size: scaned size
// returns wheather malloc succeeded
int scan_and_malloc(MyArr* my_arr);

// fill array with some values
void fill_arr(MyArr my_arr);

// print array
void print_arr(MyArr my_arr);

int main(int argc, char const *argv[])
{
    MyArr my_arr;

    int rv = scan_and_malloc(&my_arr);
    // check return values
    if (rv != 0)
    {
        return rv;
    }

    // fill array
    fill_arr(my_arr);

    // print array
    print_arr(my_arr);

    // free memory
    free(my_arr.arr);

    return EXIT_SUCCESS;
}


int scan_and_malloc(MyArr* my_arr)
{
    int size;
    int rv = scanf("%d", &size);

    // check inputs
    if (rv != 1)
    {
        fprintf(stderr,"ERROR: velikost neni cislo\n");
        return ERROR_INPUT;
    } 
    else if (size < 0)
    {
        fprintf(stderr,"ERROR: velikost je zaporna\n");
        return ERROR_NEGATIVE;
    }

    // dynamic memoery allocation
    TYPE* data = (TYPE*) malloc(size*sizeof(TYPE));
    
    // allocation check
    if (data == NULL)
    {
        // if data are null, something went horribly wrong
        fprintf(stderr, "ERROR: memory alloc failed\n");
        exit(ERROR_MEM_FAIL);
    }

    // set values to struct
    my_arr->arr = data;
    my_arr->size = size;

    return 0;
}

void fill_arr(MyArr my_arr)
{
    for(int i = 0; i < my_arr.size; ++i)
    {
        my_arr.arr[i] = 1 << i;
    }
}

void print_arr(MyArr my_arr)
{
    for (int i = 0; i < my_arr.size; i++)
    {
        printf("%d ", my_arr.arr[i]);
    }
    putchar('\n');
}
