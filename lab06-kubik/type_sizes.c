#include<stdio.h>

#define ARR_SIZE 3

// try with char, short, int, long
// this yet another way how to use MACROs
#define TYPE long

int main(int argc, char const *argv[])
{
    TYPE arr[ARR_SIZE];

    printf("%p - address of arr \n", arr);
    printf("%p - address of arr[0] \n", &arr[0]);
    printf("%p - address of arr[1] \n", &arr[1]);
    printf("%p - address of arr[2] \n", &arr[2]);
    printf("%ld - sizeof(TYPE)\n", sizeof(TYPE));

    return 0;
}