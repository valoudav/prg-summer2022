#include<stdio.h>

int main(int argc, char const *argv[])
{   
    int ss_arr[3] = {3,8,9};

    // ekvivalent k:
    /*
    ss_arr[0] = 3;
    ss_arr[1] = 8;
    ss_arr[2] = 9;
    */

    for (int i = 0; i < 3; i++)
    {
        printf("%d ",ss_arr[i]);
    }
    // stejne jako printf("%c", '\n'); printf("\n")
    putchar('\n');
    
    return 0;
}
