# 6. cvičení - Dynamická alokace, práce se soubory

## Pole statické délky (Opakování)

- velikost je třeba znát při kompilaci
- použití makra pro definice jeho velikosti
- příklady uvedeny v `ss_arr.c` a `ss_arr2.c`

### Soubor `ss_arr.c`

```c
#include<stdio.h>

int main(int argc, char const *argv[])
{    
    int ss_arr[3] = {3,8,9};

    // ekvivalent k:
    /*
    ss_arr[0] = 3;
    ss_arr[1] = 8;
    ss_arr[2] = 9;
    */

    for (int i = 0; i < 3; i++)
    {
        printf("%d ",ss_arr[i]);
    }
    // stejne jako printf("%c", '\n'); printf("\n")
    putchar('\n');
    
    return 0;
}
```

### Soubor `ss_arr2.c`

```c
#include<stdio.h>

#define ARR_SIZE 5

int main(int argc, char const *argv[])
{
    int arr[ARR_SIZE];

    for (int i = 0; i < ARR_SIZE; i++)
    {
        arr[i] = 1 << i;
    }

    // nějaký komplexní výpočet na polem,
    // jinak moc nedává smysl používat pole
    // a můžeme provádět výpočty rovnou

    for (int i = 0; i < ARR_SIZE; i++)
    {
        printf("%d ", arr[i]);
    }
    putchar('\n');   

    return 0;
}

```

## Pole variabilní délky (Opakování)

- velikost se určí za běhu programu
- podobně jako pole statické délky není možné vracet z funkce
  - paměť je na stacku
  - je možné předat pole do funkce (viz `vs_arr_fcn.c`) a v ní ho modifikovat

### Soubor `vs_arr.c`

```c
#include<stdio.h>

#define ERROR_SIZE 100
#define ERROR_INPUT 101

int main(int argc, char const *argv[])
{

    // size processing
    int size;
    printf("zadej delku sekvence:\n");
    int rv = scanf("%d", &size);

    // checking input
    if (rv != 1 || size < 0)
    {
        fprintf(stderr,"ERROR: nevalidni velikost\n");
        return ERROR_SIZE;
    }

    // defining array of loaded size
    int ss_arr[size];

    // filling array with user input
    for (int i = 0; i < size; i++)
    {
        // scanning input
        printf("zadej cislo %d sekvence\n:",i);
        int rv = scanf("%d", &ss_arr[i]);

        // checking the input
        if (rv != 1)
        {
            fprintf(stderr,"ERROR: nevalidni cislo\n");
            return ERROR_INPUT;
        }
    }
    
    // printing sequence
    for (int i = 0; i < size; i++)
    {
        printf("%d ",ss_arr[i]);
    }
    putchar('\n');

    return 0;
}

```

### Soubor `vs_arr_fcn.c`

```c
#include<stdio.h>

#define ERROR_SIZE 100

// fill an array of given size
void fill_arr(int arr[], int size);

int main(int argc, char const *argv[])
{
    // get the array size
    int size;
    printf("arr size: \n");
    int ret_val = scanf("%d", &size);
    
    // checking input
    if (rv != 1 || size < 0)
    {
        fprintf(stderr,"ERROR: nevalidni velikost\n");
        return ERROR_SIZE;
    }

    // variable size array
    int arr[size];
    
    // filling array with some values
    fill_arr(arr, size);

    // print
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    putchar('\n');   
        
    return 0;
}

void fill_arr(int arr[], int size)
{
    for (int i = 0; i < size; i++)
    {
        arr[i] = i;
    }    
}

```

## Model paměti

- zkoumání fungování paměti je zkoumáno v souboru `type_sizes.c` a `adresses.c`
- operátor `&a` aka _kde bydlí proměnná_
  - vrací adresu, kde je uložená proměnná `a`
- operátor `*p` aka _co bydlí na adrese_
  - vrací hodnotu ulozenou na adrese pointeru `p`
- operátor `&` a `*` mají opačný efekt
- formátování adresy pointeru `int* ptr;` pomocí `%p`: `printf("%p \n", ptr);`

### Soubor `type_sizes.c`

- zaměřte se na adresy a jejich rozdíl v rámci pole v závislosti na zvoleném typu
- zaměřte se na možný zůsob použití makra 
- addresy budou pro různé běhy různé

```c
#include<stdio.h>

#define ARR_SIZE 3

// try with char, short, int, long
// this yet another way how to use MACROs
#define TYPE long

int main(int argc, char const *argv[])
{
    TYPE arr[ARR_SIZE];

    printf("%p - address of arr \n", arr);
    printf("%p - address of arr[0] \n", &arr[0]);
    printf("%p - address of arr[1] \n", &arr[1]);
    printf("%p - address of arr[2] \n", &arr[2]);
    printf("%ld - sizeof(TYPE)\n", sizeof(TYPE));

    return 0;
}

```

příklad výstupu pro `#define TYPE char`:
```text
0x7ffd2635f385 - address of arr 
0x7ffd2635f385 - address of arr[0] 
0x7ffd2635f386 - address of arr[1] 
0x7ffd2635f387 - address of arr[2] 
1 - sizeof(TYPE)
```

příklad výstupu pro `#define TYPE short`:
```text
0x7ffe798d62e2 - address of arr 
0x7ffe798d62e2 - address of arr[0] 
0x7ffe798d62e4 - address of arr[1] 
0x7ffe798d62e6 - address of arr[2] 
2 - sizeof(TYPE)
```

příklad výstupu pro `#define TYPE int`:
```text
0x7ffd745b2f0c - address of arr 
0x7ffd745b2f0c - address of arr[0] 
0x7ffd745b2f10 - address of arr[1] 
0x7ffd745b2f14 - address of arr[2] 
4 - sizeof(TYPE)
```

příklad výstupu pro `#define TYPE long`:
```text
0x7ffebc7f9680 - address of arr 
0x7ffebc7f9680 - address of arr[0] 
0x7ffebc7f9688 - address of arr[1] 
0x7ffebc7f9690 - address of arr[2] 
8 - sizeof(TYPE)
```

### Soubor `adresses.c`

- povšimněte si různého způsobu adresování pole
-  `*(arr2+1)` je to samé jako `arr2[1]`
  - C je silně typovaný jazyk, takže víme velikost jednoho prvku (`sizeof()`) a je tedy možné skákat pamětí oběma způsoby


```c
#include<stdio.h>

#define TYPE int

int main(int argc, char const *argv[])
{
    // dynamic memory allocation
    TYPE arr[3] = {3,8,9};

    // print valudes using pointer arithmetics
    printf("value   %d == %d\n", *arr, arr[0]);
    printf("address %p == %p\n", arr, &arr[0]);
    printf("value   %d == %d\n", *(arr+1), arr[1]);
    printf("address %d == %d\n", arr+1, &arr[1]);
    //*(arr2+2*sizeof(int)) ... 
    printf("value   %d == %d\n", *(arr+2), arr[2]);
    printf("address %d == %d\n", arr+2, &arr[2]);

    return 0;
}

```

## Dynamická alokace paměti

- používáme funkci `void* malloc(<size in bytes>)` z knihovny `stdlib.h`
  - přijímá velikost v bajtech, takže typicky
   `malloc(arr_size*sizeof(int))`
  - vrací `void*`, takže typicky přetypováváme: `(int*)malloc(arr_size*sizeof(int))`

- paměť je třeba uvolnit pomocí `free(ptr)`
- je třeba kontrolovat vrácenou adresu
  - pokud je `NULL`, něco se nepovedlo a je třeba ukončit program
  - v případě, že je adresa `NULL` program končí s návratovou hodnotou o jedna větší než hodnota specifikovaná zadáním
  - kontrola není prováděna BRUTE, ale manuálně 

- příklad práce s dynamickou alokací:

```c
// alokujeme paměť
int* data = (int*) malloc((size)*sizeof(int));

// kontrola, jestli alokace proběhl v pořádku
if (data == NULL)
{
    // <free all other pointers>

    // když alokace neproběhne v pořádku, něco je šeredně špatně
    fprintf(stderr, "ERROR: memory alloc failed\n");
    exit(ERROR_MEM_FAIL);
}

//... magie výpočtu ... //

free(data);

```

### Soubor `da_arr.c`

- chceme-li ve funkci měnit trvale hodnotu jejího vstupního argumentu, nestačí do fce předat hodnotu proměnné, musíme předat pointer (adresu), kde je fce uložená

![pointer to int meme](https://i.redd.it/kh726uczjnq71.png)

- je-li touto hodnotou pointer (trvalá změna adresy), je třeba předat pointer na pointer

![pointer to pointer to int meme](https://i.redd.it/swszegbgpbr71.jpg)

- povšimněte si hlavičky:

```c
int scan_and_malloc(TYPE** arr, int* size);
```

- ... i jejího volání:

```c
int size;
TYPE* data;
int rv = scan_and_malloc(&data, &size);
```

Zdrojový kód:

```c
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

#define TYPE short

// ERRORS by task
#define ERROR_INPUT 100
#define ERROR_NEGATIVE 101

// memory fail error, in sequence to previous
#define ERROR_MEM_FAIL 102

// Ask user about arr size and allocate it
// - arr: pointer to data
// - size: scaned size
// returns wheather malloc succeeded
int scan_and_malloc(TYPE** arr, int* size);

// fill array with some values
void fill_arr(TYPE* arr, int size);

// print array
void print_arr(TYPE* arr, int size);

int main(int argc, char const *argv[])
{
    int size;
    TYPE* data;

    int rv = scan_and_malloc(&data, &size);
    // check return values
    if (rv != 0)
    {
        return rv;
    }

    // fill array
    fill_arr(data,size);

    // print array
    print_arr(data,size);

    // free memory
    free(data);

    return EXIT_SUCCESS;
}


int scan_and_malloc(TYPE** arr, int* size)
{
    int rv = scanf("%d", size);

    // check inputs
    if (rv != 1)
    {
        fprintf(stderr,"ERROR: velikost neni cislo\n");
        return ERROR_INPUT;
    } 
    else if (size < 0)
    {
        fprintf(stderr,"ERROR: velikost je zaporna\n");
        return ERROR_NEGATIVE;
    }

    // dynamic memoery allocation
    TYPE* data = (TYPE*) malloc((*size)*sizeof(TYPE));
    
    // allocation check
    if (data == NULL)
    {
        // if data are null, something went horribly wrong
        fprintf(stderr, "ERROR: memory alloc failed\n");
        exit(ERROR_MEM_FAIL);
    }

    // set value of arr
    *arr = data;

    return 0;
}

void fill_arr(TYPE* arr, int size)
{
    for(int i = 0; i < size; ++i)
    {
        arr[i] = 1 << i;
    }
}

void print_arr(TYPE* arr, int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    putchar('\n');
}

```

## Struktury

- parametry `int* data` a `int size` používáme společně, tedy by bylo vhodné je zabalit to struktury

- strukturu definujeme:

```c
struct My_Arr
{
    int* arr;
    int size;
};
```

- na definovaný typ referencujeme:

```c
struct My_Arr my_arr;
```

- pro snaží práci můžeme definovat ze structu typ:

```c
typedef struct My_Arr MyArr;
```

- definovaný typ pak můžeme referencovat novým jménem `MyArr`:

```c
MyArr my_arr;
```

- oboje je možné stojit do:

```c
typedef struct My_Arr
{
    TYPE* arr;
    int size;
} MyArr;
```

Soubor `da_arr.c` je upraven pro struktury v souboru `my_arr.c`.

### Soubor `my_arr.c`

- povšimněte si přístupu k proměnným pointeru do strutury:

```c
my_arr->arr = data;
my_arr->size = size;
```

Zdrojový kód:

```c
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

#define TYPE short

// ERRORS by task
#define ERROR_INPUT 100
#define ERROR_NEGATIVE 101

// memory fail error, in sequence to previous
#define ERROR_MEM_FAIL 102

// defining my structure
struct My_Arr
{
    TYPE* arr;
    int size;
};

// defining my new data type
typedef struct My_Arr MyArr;

// Ask user about arr size and allocate it
// - arr: pointer to data
// - size: scaned size
// returns wheather malloc succeeded
int scan_and_malloc(MyArr* my_arr);

// fill array with some values
void fill_arr(MyArr my_arr);

// print array
void print_arr(MyArr my_arr);

int main(int argc, char const *argv[])
{
    MyArr my_arr;

    int rv = scan_and_malloc(&my_arr);
    // check return values
    if (rv != 0)
    {
        return rv;
    }

    // fill array
    fill_arr(my_arr);

    // print array
    print_arr(my_arr);

    // free memory
    free(my_arr.arr);

    return EXIT_SUCCESS;
}


int scan_and_malloc(MyArr* my_arr)
{
    int size;
    int rv = scanf("%d", &size);

    // check inputs
    if (rv != 1)
    {
        fprintf(stderr,"ERROR: velikost neni cislo\n");
        return ERROR_INPUT;
    } 
    else if (size < 0)
    {
        fprintf(stderr,"ERROR: velikost je zaporna\n");
        return ERROR_NEGATIVE;
    }

    // dynamic memoery allocation
    TYPE* data = (TYPE*) malloc(size*sizeof(TYPE));
    
    // allocation check
    if (data == NULL)
    {
        // if data are null, something went horribly wrong
        fprintf(stderr, "ERROR: memory alloc failed\n");
        exit(ERROR_MEM_FAIL);
    }

    // set values to struct
    my_arr->arr = data;
    my_arr->size = size;

    return 0;
}

void fill_arr(MyArr my_arr)
{
    for(int i = 0; i < my_arr.size; ++i)
    {
        my_arr.arr[i] = 1 << i;
    }
}

void print_arr(MyArr my_arr)
{
    for (int i = 0; i < my_arr.size; i++)
    {
        printf("%d ", my_arr.arr[i]);
    }
    putchar('\n');
}

```