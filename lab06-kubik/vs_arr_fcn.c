#include<stdio.h>

#define ERROR_SIZE 100

// fill an array of given size with some data
// '-> idxs are used as example
void fill_arr(int arr[], int size);

int main(int argc, char const *argv[])
{
    // get the array size
    int size;
    printf("arr size: \n");
    int ret_val = scanf("%d", &size);
    
    // checking input
    if (ret_val != 1 || size < 0)
    {
        fprintf(stderr,"ERROR: nevalidni velikost\n");
        return ERROR_SIZE;
    }

    // variable size array
    int arr[size];
    
    // filling array with some values
    fill_arr(arr, size);

    // print
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    putchar('\n');   
        
    return 0;
}

void fill_arr(int arr[], int size)
{
    for (int i = 0; i < size; i++)
    {
        arr[i] = i;
    }    
}
