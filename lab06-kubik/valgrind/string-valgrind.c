#include<stdio.h>

/* copy string in orig to target and increase its value by one*/
void copy_and_increase(char* orig, char* target, int len);

int main(int argc, char const *argv[])
{
    char* s1 = "HELLO PRG!";
    int len = 10;
    char s2[len];
    copy_and_increase(s1,s2,len);
    printf("%s\n", s2);
    return 0;
}


void copy_and_increase(char* orig, char* target, int len)
{
    // NOTE THIS ...  v
    for (int i = 0; i < len; i++)
    {
        target[i]=orig[i]+1;
    }
    // ... and absence of this
    // target[len] = '\0';
}
