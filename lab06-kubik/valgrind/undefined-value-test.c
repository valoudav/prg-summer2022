#include<stdio.h>

#define INPUT_ERROR 100

int main(int argc, char const *argv[])
{
    int n,m;
    int rv = scanf("%d %d", &n, &m);

    //       v- NOTE THIS
    if (rv < 1)
    {
        fprintf(stderr, "ERROR: invalid inputs\n");
        return INPUT_ERROR;
    }

    printf("%d %d\n", n, m);
    
    return 0;
}
