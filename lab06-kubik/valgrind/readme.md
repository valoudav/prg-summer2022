# Program `valgrind` na praktických ukázkách

- program valgrind slouží ke kontrole a debugování programů z hlediska jejich práce s (dynamicky alokovanou) pamětí
- může se stát, že není součástí vaší distribuce Linuxu, na Ubuntu a jeho derivátech nainstlujete přes:
  
  ```bash
  sudo apt install valgrind -y
  ```

- uvažujme následující program v souboru `valgrind-test.c`

```c
#include<stdio.h>
#include<stdlib.h>

#define SIZE 10
#define MEMORY_ERROR 100

int main(int argc, char const *argv[])
{
    
    int* arr = (int*) malloc(SIZE*sizeof(int));

    if (arr==NULL)
    {
        fprintf(stderr, "ERROR: memory allocation failed\n");
        return MEMORY_ERROR;
    }

    // accessing array out of allocated memory
    // '-> for writing
    arr[SIZE] = 10;
    // '-> for read
    printf("%d \n", arr[SIZE]);

    // filling array
    //                 v-- NOTE this
    for (int i = 0; i <= SIZE; ++i)
    {
        arr[i] = 1 << i;
    }

    // printing array
    //                 v-- NOTE this
    for (int i = 0; i <= SIZE; i++)
    {
        printf("%d ", arr[i]);
    }
    putchar('\n');    

    // freeing memory
    // free(arr);

    return EXIT_SUCCESS;
}

```

- programy, které chcete ladit ve valgrindu kompilujte s flagem `-g` pro zobrazení čísel řádků, na kterých došlo k chybě.
  - ke kompilaci demonstračnío souboru tedy použijte například příkaz

    ```bash
    gcc valgrind-test.c -g -o valgrind-test
    ```
  - prorovnejte chování zkompilujete-li bez flagu `g`

- program v prostředí valgrind spustíte:
  
  ```bash
  valgrind ./valgrind-test
  ```
  
  - vstupy i výstupy programu spouštěného ve `valgrindu` je možné přesměrovat jako kdybychom ho pouštěli mimo valgrind:
  
    ```bash
    valgrind ./program < data.in > data.out
    ```

<table>
<tr>
<td> S přepínačem -g </td> <td> Bez přepínače -g </td>
</tr>
<tr>
<td>

```
==12834== Memcheck, a memory error detector
==12834== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==12834== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==12834== Command: ./valgrind-test
==12834== 
==12834== Invalid write of size 4
==12834==    at 0x109203: main (valgrind-test.c:20)
==12834==  Address 0x4a80068 is 0 bytes after a block of size 40 alloc'd
==12834==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==12834==    by 0x1091C5: main (valgrind-test.c:10)
==12834== 
==12834== Invalid read of size 4
==12834==    at 0x109211: main (valgrind-test.c:22)
==12834==  Address 0x4a80068 is 0 bytes after a block of size 40 alloc'd
==12834==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==12834==    by 0x1091C5: main (valgrind-test.c:10)
==12834== 
10 
==12834== Invalid write of size 4
==12834==    at 0x109251: main (valgrind-test.c:28)
==12834==  Address 0x4a80068 is 0 bytes after a block of size 40 alloc'd
==12834==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==12834==    by 0x1091C5: main (valgrind-test.c:10)
==12834== 
==12834== Invalid read of size 4
==12834==    at 0x10927A: main (valgrind-test.c:35)
==12834==  Address 0x4a80068 is 0 bytes after a block of size 40 alloc'd
==12834==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==12834==    by 0x1091C5: main (valgrind-test.c:10)
==12834== 
1 2 4 8 16 32 64 128 256 512 1024 
==12834== 
==12834== HEAP SUMMARY:
==12834==     in use at exit: 40 bytes in 1 blocks
==12834==   total heap usage: 2 allocs, 1 frees, 1,064 bytes allocated
==12834== 
==12834== LEAK SUMMARY:
==12834==    definitely lost: 40 bytes in 1 blocks
==12834==    indirectly lost: 0 bytes in 0 blocks
==12834==      possibly lost: 0 bytes in 0 blocks
==12834==    still reachable: 0 bytes in 0 blocks
==12834==         suppressed: 0 bytes in 0 blocks
==12834== Rerun with --leak-check=full to see details of leaked memory
==12834== 
==12834== For lists of detected and suppressed errors, rerun with: -s
==12834== ERROR SUMMARY: 4 errors from 4 contexts (suppressed: 0 from 0)
```

</td>
<td>

```
==14357== Memcheck, a memory error detector
==14357== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==14357== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==14357== Command: ./valgrind-test
==14357== 
==14357== Invalid write of size 4
==14357==    at 0x109203: main (in /home/user/snippets/valgrind/valgrind-test)
==14357==  Address 0x4a80068 is 0 bytes after a block of size 40 alloc'd
==14357==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==14357==    by 0x1091C5: main (in /home/user/snippets/valgrind/valgrind-test)
==14357== 
==14357== Invalid read of size 4
==14357==    at 0x109211: main (in /home/user/snippets/valgrind/valgrind-test)
==14357==  Address 0x4a80068 is 0 bytes after a block of size 40 alloc'd
==14357==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==14357==    by 0x1091C5: main (in /home/user/snippets/valgrind/valgrind-test)
==14357== 
==14357== Invalid write of size 4
==14357==    at 0x109251: main (in /home/user/snippets/valgrind/valgrind-test)
==14357==  Address 0x4a80068 is 0 bytes after a block of size 40 alloc'd
==14357==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==14357==    by 0x1091C5: main (in /home/user/snippets/valgrind/valgrind-test)
==14357== 
==14357== Invalid read of size 4
==14357==    at 0x10927A: main (in /home/user/snippets/valgrind/valgrind-test)
==14357==  Address 0x4a80068 is 0 bytes after a block of size 40 alloc'd
==14357==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==14357==    by 0x1091C5: main (in /home/user/snippets/valgrind/valgrind-test)
==14357== 
10 
1 2 4 8 16 32 64 128 256 512 1024 
==14357== 
==14357== HEAP SUMMARY:
==14357==     in use at exit: 40 bytes in 1 blocks
==14357==   total heap usage: 2 allocs, 1 frees, 4,136 bytes allocated
==14357== 
==14357== LEAK SUMMARY:
==14357==    definitely lost: 40 bytes in 1 blocks
==14357==    indirectly lost: 0 bytes in 0 blocks
==14357==      possibly lost: 0 bytes in 0 blocks
==14357==    still reachable: 0 bytes in 0 blocks
==14357==         suppressed: 0 bytes in 0 blocks
==14357== Rerun with --leak-check=full to see details of leaked memory
==14357== 
==14357== For lists of detected and suppressed errors, rerun with: -s
==14357== ERROR SUMMARY: 4 errors from 4 contexts (suppressed: 0 from 0)
```

</td>
</tr>
</table>

## Význam výpisu valgrindu

- ve všech příkladech předpokládáme kompilaci s flagem `-g`

## Ukázka 1 - zápis mimo zaalokovanou paměť

```
==12834== Invalid write of size 4
==12834==    at 0x109203: main (valgrind-test.c:20)
==12834==  Address 0x4a80068 is 0 bytes after a block of size 40 alloc'd
==12834==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==12834==    by 0x1091C5: main (valgrind-test.c:10)
```

- první řádek říka kolik bajtů bylo zapsáno
- druhý řádek říká, v jakém souboru `valgrind-test.c` a na kolikátém řádku `:20` k zápisu došlo, to odpovídá: `arr[SIZE] = 10;`
- třetí řádek říká, že jsme 0 bajtů za zaalokovaný 40 bajty, tedy na prvním indexu mimo pole
- poslední řádek pak říká, že paměť byla alokována v souboru`valgrind-test.c` a na řádku `10`, to odpovídá: `int* arr = (int*) malloc(SIZE*sizeof(int));`
- předposlední, že alokaci provedla fce `malloc`

Někdy se může stát, že zápisu mimo alokovanou paměť dochází ve cyklu:
```
==12834== Invalid write of size 4
==12834==    at 0x109251: main (valgrind-test.c:28)
==12834==  Address 0x4a80068 is 0 bytes after a block of size 40 alloc'd
==12834==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==12834==    by 0x1091C5: main (valgrind-test.c:10)
```

Pak je vidět, že sice valgrind správně identifikoval, kde k chybnému zápisu došlo, ale chyba je o dva řádky výše v podmínce for-cyklu.

## Ukázka 2 - čtení mimo zaalokovanou paměť

```
==12834== Invalid read of size 4
==12834==    at 0x109211: main (valgrind-test.c:22)
==12834==  Address 0x4a80068 is 0 bytes after a block of size 40 alloc'd
==12834==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==12834==    by 0x1091C5: main (valgrind-test.c:10)
```

- obdobně jako v předchozím případě je chyba i místo, kde dochází k přístupu mimo zaalokovanou paměť na stejném místě, tj. na řádku 22: `printf("%d \n", arr[SIZE]);`
- ostatní atributy jsou stejné

```
==12834== Invalid read of size 4
==12834==    at 0x10927A: main (valgrind-test.c:35)
==12834==  Address 0x4a80068 is 0 bytes after a block of size 40 alloc'd
==12834==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==12834==    by 0x1091C5: main (valgrind-test.c:10)
```

- v případě for-cyklu platí pro čtení to samé, co pro zápis

## Ukázka 3 - HEAP a LEAK SUMMARY

- pro zjitění, kolik paměti bylo celkem zaalokováno (`malloc`,`calloc`,`realloc`) a kolik jí byl uvolněno (`free`,`realloc`) slouží `HEAP SUMMARY` v samém závěru výpisu:

```
==12834== HEAP SUMMARY:
==12834==     in use at exit: 40 bytes in 1 blocks
==12834==   total heap usage: 2 allocs, 1 frees, 1,064 bytes allocated
```

Jak je vidět, chybí jedno uvolnění paměti (2 alloc, 1 frees), ergo existují nějaké memory LEAKY:

```
==12834== LEAK SUMMARY:
==12834==    definitely lost: 40 bytes in 1 blocks
==12834==    indirectly lost: 0 bytes in 0 blocks
==12834==      possibly lost: 0 bytes in 0 blocks
==12834==    still reachable: 0 bytes in 0 blocks
==12834==         suppressed: 0 bytes in 0 blocks
```

pole `int`ů o velikosti `10`, což odpovídá poli alokovanému na řádku 10.

Pokud odkomentujeme řádek 40, znovu zkompilujeme a pustíme ve `valgrind`u už žádné LEAKY nemáme.

### Ukázka 4 - unitialized values

- v případě, že dojde k nedostatečnému ošetření vstupů může dojít k `Use of uninitialised value`.
- Uvažujme kód v souboru `undefined-value-test.c`:

```c
#include<stdio.h>

#define INPUT_ERROR 100

int main(int argc, char const *argv[])
{
    int n,m;
    int rv = scanf("%d %d", &n, &m);

    //       v- NOTE THIS
    if (rv < 1)
    {
        fprintf(stderr, "ERROR: invalid inputs\n");
        return INPUT_ERROR;
    }

    printf("%d %d\n", n, m);
    
    return 0;
}

```

- kód přeložíme příkazem: `gcc undefined-value-test.c -g -o undefined-value-test`

- pro vstup `4 d` vrátí `valgrind ./undefined-value-test` výpis:

```
==17451== Memcheck, a memory error detector
==17451== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==17451== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==17451== Command: ./undefined-value-test
==17451== 
4 d
==17451== Conditional jump or move depends on uninitialised value(s)
==17451==    at 0x4902988: __vfprintf_internal (vfprintf-internal.c:1687)
==17451==    by 0x48ECD6E: printf (printf.c:33)
==17451==    by 0x10922F: main (undefined-value-test.c:16)
==17451== 
==17451== Use of uninitialised value of size 8
==17451==    at 0x48E66CB: _itoa_word (_itoa.c:179)
==17451==    by 0x49025A4: __vfprintf_internal (vfprintf-internal.c:1687)
==17451==    by 0x48ECD6E: printf (printf.c:33)
==17451==    by 0x10922F: main (undefined-value-test.c:16)
==17451== 
==17451== Conditional jump or move depends on uninitialised value(s)
==17451==    at 0x48E66DD: _itoa_word (_itoa.c:179)
==17451==    by 0x49025A4: __vfprintf_internal (vfprintf-internal.c:1687)
==17451==    by 0x48ECD6E: printf (printf.c:33)
==17451==    by 0x10922F: main (undefined-value-test.c:16)
==17451== 
==17451== Conditional jump or move depends on uninitialised value(s)
==17451==    at 0x4903258: __vfprintf_internal (vfprintf-internal.c:1687)
==17451==    by 0x48ECD6E: printf (printf.c:33)
==17451==    by 0x10922F: main (undefined-value-test.c:16)
==17451== 
==17451== Conditional jump or move depends on uninitialised value(s)
==17451==    at 0x490271E: __vfprintf_internal (vfprintf-internal.c:1687)
==17451==    by 0x48ECD6E: printf (printf.c:33)
==17451==    by 0x10922F: main (undefined-value-test.c:16)
==17451== 
4 -16778048
==17451== 
==17451== HEAP SUMMARY:
==17451==     in use at exit: 0 bytes in 0 blocks
==17451==   total heap usage: 2 allocs, 2 frees, 2,048 bytes allocated
==17451== 
==17451== All heap blocks were freed -- no leaks are possible
==17451== 
==17451== Use --track-origins=yes to see where uninitialised values come from
==17451== For lists of detected and suppressed errors, rerun with: -s
==17451== ERROR SUMMARY: 19 errors from 5 contexts (suppressed: 0 from 0)
```

Dalším příkladem může být chybné pořadí částí podmínke, viz následující příklad:

```c
int input;
int rv = scanf("%d", &input);
if ((input == 0) && (rv == 1))
{
  /* DO STUFF */
}

```

kde pro nevalidní vstup (třeba `b`) skutečně hodnota výrazu `(input == 0)` závisí na hodnotě `input`, která nebyla inicializována.

### Ukázka 5 - unitialized values a char*

Při práci s textovými řetezci může dojít k opomenutí ukončovacího znaku `\0`, což může tak způsobit _Conditional jump or move depends on uninitialised value(s)_ zpravidla na řádku, kde doje k výpisu.

Uvažujme program `string-valgrind`:

```c
#include<stdio.h>

/* copy string in orig to target and increase its value by one*/
void copy_and_increase(char* orig, char* target, int len);

int main(int argc, char const *argv[])
{
    char* s1 = "HELLO PRG!";
    int len = 10;
    char s2[len];
    copy_and_increase(s1,s2,len);
    printf("%s\n", s2);
    return 0;
}


void copy_and_increase(char* orig, char* target, int len)
{
    // NOTE THIS ...  v
    for (int i = 0; i < len; i++)
    {
        target[i]=orig[i]+1;
    }
    // ... and absence of this
    // target[len] = '\0';
}

```

pro který `valgrind` vrátí mimojiné výstup podobný tomuto:

```txt
==16286== Conditional jump or move depends on uninitialised value(s)
==16286==    at 0x483EF58: strlen (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==16286==    by 0x490F463: puts (ioputs.c:35)
==16286==    by 0x109269: main (string-valgrind.c:12)
```

Problém je v chybějícím terminačním symbolu, který se ovšem projeví až ve chvíli, kdy se pokoušíme textový řetězec vytisknout.

## Užitečné přepínače

- `--track-fds=yes` kontroluje nezavřené soubory
- `--track-origins=yes` hledá původ neinicializované proměnné
- `--leak-check=full` umožňuje sledovat, kde byl zaalokován blok paměti, který nebyl uvolněn. V případě `valgrind-test.c`:

```
==12834== 40 bytes in 1 blocks are definitely lost in loss record 1 of 1
==12834==    at 0x483B7F3: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==12834==    by 0x1091C5: main (valgrind-test.c:10)
```
