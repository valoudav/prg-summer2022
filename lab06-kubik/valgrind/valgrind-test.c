#include<stdio.h>
#include<stdlib.h>

#define SIZE 10
#define MEMORY_ERROR 100

int main(int argc, char const *argv[])
{
    
    int* arr = (int*) malloc(SIZE*sizeof(int));

    if (arr==NULL)
    {
        fprintf(stderr, "ERROR: memory allocation failed\n");
        return MEMORY_ERROR;
    }

    // accessing array out of allocated memory
    // '-> for writing
    arr[SIZE] = 10;
    // '-> for read
    printf("%d \n", arr[SIZE]);

    // filling array
    //                 v-- NOTE this
    for (int i = 0; i <= SIZE; ++i)
    {
        arr[i] = 1 << i;
    }

    // printing array
    //                 v-- NOTE this
    for (int i = 0; i <= SIZE; i++)
    {
        printf("%d ", arr[i]);
    }
    putchar('\n');    

    // freeing memory
    // free(arr);

    return EXIT_SUCCESS;
}

