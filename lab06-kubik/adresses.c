#include<stdio.h>

#define TYPE int

int main(int argc, char const *argv[])
{
    // dynamic memory allocation
    TYPE arr[3] = {3,8,9};

    // print valudes using pointer arithmetics
    printf("value   %d == %d\n", *arr, arr[0]);
    printf("address %p == %p\n", arr, &arr[0]);
    printf("value   %d == %d\n", *(arr+1), arr[1]);
    printf("address %d == %d\n", arr+1, &arr[1]);
    //*(arr2+2*sizeof(int)) ... 
    printf("value   %d == %d\n", *(arr+2), arr[2]);
    printf("address %d == %d\n", arr+2, &arr[2]);

    return 0;
}
