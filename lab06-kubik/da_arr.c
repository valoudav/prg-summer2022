#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

#define TYPE short

// ERRORS by task
#define ERROR_INPUT 100
#define ERROR_NEGATIVE 101

// memory fail error, in sequence to previous
#define ERROR_MEM_FAIL 102

// Ask user about arr size and allocate it
// - arr: pointer to data
// - size: scaned size
// returns wheather malloc succeeded
int scan_and_malloc(TYPE** arr, int* size);

// fill array with some values
void fill_arr(TYPE* arr, int size);

// print array
void print_arr(TYPE* arr, int size);

int main(int argc, char const *argv[])
{
    int size;
    TYPE* data;

    int rv = scan_and_malloc(&data, &size);
    // check return values
    if (rv != 0)
    {
        return rv;
    }

    // fill array
    fill_arr(data,size);

    // print array
    print_arr(data,size);

    // free memory
    free(data);

    return EXIT_SUCCESS;
}


int scan_and_malloc(TYPE** arr, int* size)
{
    int rv = scanf("%d", size);

    // check inputs
    if (rv != 1)
    {
        fprintf(stderr,"ERROR: velikost neni cislo\n");
        return ERROR_INPUT;
    } 
    else if (*size < 0)
    {
        fprintf(stderr,"ERROR: velikost je zaporna\n");
        return ERROR_NEGATIVE;
    }

    // dynamic memoery allocation
    TYPE* data = (TYPE*) malloc((*size)*sizeof(TYPE));
    
    // allocation check
    if (data == NULL)
    {
        // if data are null, something went horribly wrong
        fprintf(stderr, "ERROR: memory alloc failed\n");
        exit(ERROR_MEM_FAIL);
    }

    // set value of arr
    *arr = data;

    return 0;
}

void fill_arr(TYPE* arr, int size)
{
    for(int i = 0; i < size; ++i)
    {
        arr[i] = 1 << i;
    }
}

void print_arr(TYPE* arr, int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    putchar('\n');
}
