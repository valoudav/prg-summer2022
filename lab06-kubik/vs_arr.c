#include<stdio.h>

#define ERROR_SIZE 100
#define ERROR_INPUT 101

int main(int argc, char const *argv[])
{

    // size processing
    int size;
    printf("zadej delku sekvence:\n");
    int rv = scanf("%d", &size);

    // checking input
    if (rv != 1 || size < 0)
    {
        fprintf(stderr,"ERROR: nevalidni velikost\n");
        return ERROR_SIZE;
    }

    // defining array of loaded size
    int ss_arr[size];

    // filling array with user input
    for (int i = 0; i < size; i++)
    {
        // scanning input
        printf("zadej cislo %d sekvence\n:",i);
        int rv = scanf("%d", &ss_arr[i]);

        // checking the input
        if (rv != 1)
        {
            fprintf(stderr,"ERROR: nevalidni cislo\n");
            return ERROR_INPUT;
        }
    }
    
    // printing sequence
    for (int i = 0; i < size; i++)
    {
        printf("%d ",ss_arr[i]);
    }
    putchar('\n');

    return 0;
}
