#include <stdio.h>

static char* day_of_week[] = {"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su" };
static char *name_of_month[] = {
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
};
static int days_in_month[] = { 31, 28, 31, 30, 31, 30,
                               31, 31, 30, 31, 30, 31 };
static int first_day_in_march = 1; // 1. 3. 2022 is Tuesday
static int first_day_in_year = 5; // 1. 1. 2022 is Saturday

void print_header()
{
    for(int i = 0 ; i < 7; ++i) {
        printf("%s ", day_of_week[i]);
    }
    putchar('\n');
}

void print_days_table(int first_day, int num_days)
{
    for(int i = 1 - first_day; i <= num_days; ++i) {
        if(i <= 0) {
            printf("   ");
        } else {
            printf("%2d ", i);
            if((i+first_day) % 7 == 0) {
                putchar('\n');
            }
        }
    }
    putchar('\n');
}

void print_month(int month)
{
    printf("%s\n", name_of_month[month]);
    print_header();
    int first_day = first_day_in_year;
    for(int i = 0; i < month; ++i) {
        first_day = first_day + days_in_month[i];
    }
    first_day = first_day % 7;
    print_days_table(first_day, days_in_month[month]);
}

int main()
{
    print_month(10);

    return 0;
}