#include <stdio.h>

int sum(int a, int b);
void print_seq(int beg, int end);

int main()
{
    int a;
    int arr[] = {1,2,3,4,5};
    arr[2] = 30;
    printf("%d\n", arr[2]);
    int *p = arr;
    printf("%d\n", *(p+2));




    int i = 1;
    int n = 10;
    while(i <= n) {
        printf("%d ", i);
        i = i + 1;
    }
    printf("\n");

    print_seq(1, 10);

    return 0;
}

void print_seq(int beg, int end)
{
    if(beg > end) {
        printf("\n");
        return;
    }

    printf("%d ", beg);
    print_seq(beg + 1, end);
}

int sum(int a, int b)
{
    a = a + 5;
    return a + b;
}