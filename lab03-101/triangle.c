#include <stdio.h>

/* Possible exit values */
#define EXIT_SUCCESS 0
#define ERROR_INPUT 100
#define ERROR_RANGE 101

#define MIN_VALUE 1
#ifndef MAX_VALUE
#define MAX_VALUE 10
#endif

int read_input(int *n);
void print_triangle(char c, int lines);
void print_line(char c, int n);
void print_error(int ret, int n);

int main()
{
    int v; 
    int ret = read_input(&v);

    if (ret == EXIT_SUCCESS) {
        printf("You entered: %d\n", v);
        print_triangle('*', v);
    }

    print_error(ret, v);

    return ret;
}

int read_input(int *n)
{
    int ret = EXIT_SUCCESS;
    printf("Enter integer in range %d to %d: ", MIN_VALUE, MAX_VALUE);
    int r = scanf("%d", n);
    if (r != 1) {
        ret = ERROR_INPUT;
    } else if (*n < MIN_VALUE || *n > MAX_VALUE) {
        ret = ERROR_RANGE;
    }
    return ret;
}

void print_error(int ret, int n)
{
        switch (ret) {
        case ERROR_INPUT:
            printf("ERROR: Cannot read integer value from stdin.\n");
            break;
        case ERROR_RANGE:
            printf("ERROR: Given value %d is outside of the range [%d, %d].\n", n, MIN_VALUE, MAX_VALUE);
            break;
    }
}

void print_triangle(char c, int lines)
{
    for(int i = 0; i < lines; ++i) {
        print_line(c, lines - i);
    }
}

void print_line(char c, int n)
{
    for(int i = 0; i < n; ++i) {
        putchar(c);
    }
    putchar('\n');
}
