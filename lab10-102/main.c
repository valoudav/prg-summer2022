#include <stdio.h>
#include <stdbool.h>

#include <unistd.h>
#include <pthread.h>
#include <termios.h>

void* thread1(void*);
void* thread2(void*);

void set_raw(int set);

void* input_thrd(void*);
void* output_thrd(void*);
void* counter_thrd(void*);

enum {OUTPUT, INPUT, COUNTER, N_THREADS};
char* thrd_names[] = {"output", "input", "counter"};
void *(*(thrd_funs[]))(void*) = {output_thrd, input_thrd, counter_thrd};
pthread_t threads[N_THREADS];

pthread_mutex_t counter_mtx;
pthread_cond_t counter_cond;
int counter;
bool quit = false;

int main(void)
{
   pthread_mutex_init(&counter_mtx, NULL);
   pthread_cond_init(&counter_cond, NULL);
   counter = 0;

   set_raw(true);

   for(int i = 0; i < N_THREADS; ++i) {
      int r = pthread_create(&threads[i], NULL, thrd_funs[i], NULL);
      printf("Creating thread '%s': %s\n", thrd_names[i], (r == 0 ? "OK" : "FAIL"));
   }

   // getchar();
   // quit = true;

   for(int i = 0; i < N_THREADS; ++i) {
      int r = pthread_join(threads[i], NULL);
      printf("Joining thread '%s': %s\n", thrd_names[i], (r == 0 ? "OK" : "FAIL"));
   }

   set_raw(false);
   return 0;
}

void* thread1(void *v)
{
   bool q = false;
   for(int i = 0; i < 20; ++i) {
      usleep(10 * 1000);

      pthread_mutex_lock(&counter_mtx);

      int c = counter;
      usleep(100);
      c += 1;
      counter = c;
      pthread_cond_signal(&counter_cond);
      pthread_mutex_unlock(&counter_mtx);
   }
   // while (!q) {
   //    usleep(100 * 1000);
   //    counter += 1;
   //    q = quit;
   // }
   return 0;
}

void* thread2(void *v) 
{
   bool q = false;
   int n_prints = 0;
   while (!q) {
      pthread_mutex_lock(&counter_mtx);
      pthread_cond_wait(&counter_cond, &counter_mtx);

      printf("\rCounter %10i", counter);
      n_prints += 1;
      fflush(stdout);

      pthread_mutex_unlock(&counter_mtx);
      q = quit;
   }
   printf("\rCounter %10i\n", counter);
   printf("n_prints: %10i\n", n_prints);
   return 0;
}

void* input_thrd(void* arg)
{
   bool q = false;
   while(!q) {
      int c = getchar();
      switch (c) {
      case 'q':
         quit = true;
         break;
      default:
         break;
      }

      q = quit;
   }
   return 0;
}

void* output_thrd(void* arg)
{
   bool q = false;
   while (!q) {
      pthread_mutex_lock(&counter_mtx);
      pthread_cond_wait(&counter_cond, &counter_mtx);

      printf("\rCounter %10i", counter);
      fflush(stdout);

      pthread_mutex_unlock(&counter_mtx);
      q = quit;
   }
   printf("\rCounter %10i\n", counter);
   return 0;
}

void* counter_thrd(void* arg)
{
   bool q = false;
   while (!q) {
      usleep(100 * 1000);
      counter += 1;
      pthread_cond_signal(&counter_cond);
      q = quit;
   }
   return 0;
}

void set_raw(int set)
{
   static struct termios tio, tioOld;
   tcgetattr(STDIN_FILENO, &tio);
   if (set) {
      tioOld = tio; //backup 
      cfmakeraw(&tio);
      tio.c_lflag &= ~ECHO; // assure echo is disabled
      tio.c_oflag |= OPOST; // enable output postprocessing
      tcsetattr(STDIN_FILENO, TCSANOW, &tio);
   } else {
      tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
   }
}