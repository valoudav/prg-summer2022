#include <stdio.h>

int mystrlen(char *str);
int mystrcmp(char *str1, char *str2);

int main()
{
    char *str = "everybody likes PRG";
    char str2[] = "Everybody likes PRGA";

    int size = mystrlen(str);
    printf("strlen(str): %d\n", size);
    printf("strcmp(str, str2): %d\n", mystrcmp(str, str2));

    printf("%s\n", str);
    printf("%s\n", str2);
    return 0;
}

int mystrcmp(char *str1, char *str2)
{
    int i;
    for(i = 0; str1[i] != '\0' && str2[i] != '\0' && str1[i] == str2[i]; ++i);

    if(str1[i] < str2[i]) {
        return -1;
    } else if (str1[i] == str2[i]) {
        return 0;
    } else {
        return 1;
    }
}

int mystrlen(char *str)
{
    int size = 0;
    while(str[size] != '\0') {
        size += 1;
    }
    return size;
}