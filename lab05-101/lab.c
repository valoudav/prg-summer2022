#include <stdio.h>

void load_arr(int *arr, int size);
void print_arr(int *arr, int size);

void print_mat(int* m, int h, int w)
{
    int (*(m2[w]));
    int ((*m2d)[w]) = (int(*)[])m;
    for(int i = 0; i < h; ++i) {
        for(int j = 0; j < w; ++j) {
            // printf("%d ", m[i*w + j]);
            printf("%d ", m2d[i][j]);
        }
        putchar('\n');
    }
}

int main(void)
{
    int arr[] = {1,2,3,4,5,6};
    print_mat(arr, 2, 3);

    putchar('\n');
    int ((mat[2])[3]) = {{1,2,3},{4,5,6}};
    for(int i = 0; i < 2; ++i) {
        for(int j = 0; j < 3; ++j) {
            printf("%d ", mat[i][j]);
        }
        putchar('\n');
    }
    int *map1d = (int*)mat;
    for(int i = 0; i < 6; ++i) {
        printf("%d ", map1d[i]);
    }
    putchar('\n');


    int n;
    if(scanf("%d", &n) != 1) {
        return 100;
    }
    int vaarr[n];
    load_arr(vaarr, n);
    print_arr(vaarr, n);
    printf("sizeof(int): %lu\n", sizeof(int));
    printf("sizeof(char): %lu\n", sizeof(char));
    printf("sizeof(vaarr): %lu\n", sizeof(vaarr));
    printf("len(vaarr): %lu\n", sizeof(vaarr)/sizeof(int));





    return 0;
}

void load_arr(int *arr, int size)
{
    printf("sizeof(arr)(in load_arr()): %lu\n", sizeof(arr));
    for(int i = 0; i < size; ++i) {
        scanf("%d", &arr[i]);
    }
}

void print_arr(int *arr, int size)
{
    for(int i = 0; i < size; ++i) {
        printf("%d ", arr[i]);
    }
        putchar('\n');

}