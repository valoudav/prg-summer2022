#include <stdio.h>

#define N 10

int main()
{
    for(int i = 0; i < N; ++i) {
        for(int j = 0; j < N; ++j) {
            if(i == j && i + j == N - 1) {
                printf("%2d ", i * N + j);
            }else{
                printf("   ");
            }
        }
        printf("\n");
    }


    return 0;

    int size = 5;
    for(int i = 0; i < size; i++) {
        for(int j = 0 ; j < size - i ; j++) {
            putchar("*");
        }
        printf("\n");
    }

    return 0;

    char c;
    c = getc(stdin);
    printf("as char: %c\n", c);
    printf("as decimal: %d\n", c);
    printf("as hex: 0x%x\n", c);
    printf("sizeof(char): %lu\n", sizeof(char));
    printf("as binary: b:");
    unsigned char mask = 0x80;
    for (int i = 0 ;i < 8;i++){
        char r = mask & c;
        mask = mask >> 1;
        printf("%i", r > 0);
    }
    printf("\n");

    return 0;

    int n;
    if(scanf("%d", &n) == 1) {
        for(int i = 1; i <= n; ++i) {
            if(i % 2 != 0) {
                continue;
            }
            printf("%d ", i);
        }
        printf("\n");
    } else {
        return 1;
    }


    return 0;









    int i = 1;
    //while (i <= 10)
    for(;;)
    {
        if(i > N) {
            break;
        }
        printf("%d ", i);
        i++;
    }
    printf("\n");
    i = 1;
    do {
        printf("%d ", i);
        i++;
    } while(i <= N);

    printf("\n");

    return 0;
}