#include <stdio.h>

#define EXIT_SUCCESS 0
#define ERROR_INPUT 100
#define ERROR_RANGE 101

#define MIN_VALUE 1
#ifndef MAX_VALUE
#define MAX_VALUE 10
#endif

int read_input(int *n);
void print_error(int ret, int n);
void print_triangle(char c, int lines);
void print_line(char c, int n);

int main()
{
    int n;
    int ret = read_input(&n);

    if (ret == EXIT_SUCCESS) {
        printf("You entered: %d\n", n);
        print_triangle('*', n);
    }

    print_error(ret, n);

    return ret;
}

int read_input(int *n)
{
    int ret = EXIT_SUCCESS;
    printf("Enter integer in range [%d, %d]: ", MIN_VALUE, MAX_VALUE);
    int r = scanf("%d", n);

    if(r != 1) {
        ret = ERROR_INPUT;
    } else if (*n < MIN_VALUE || *n > MAX_VALUE) {
        ret = ERROR_RANGE;
    }

    return ret;
}

void print_error(int ret, int n)
{
    switch (ret) {
        case ERROR_INPUT:
            fprintf(stderr, "ERROR: Cannot read integer value from stdin.\n");
            break;
        case ERROR_RANGE:
            fprintf(stderr, "ERROR: Given value %d is not within range [%d, %d].\n", n, MIN_VALUE, MAX_VALUE);
            break;
    }
}

void print_triangle(char c, int lines)
{
    for(int i = 0; i < lines; ++i) {
        print_line(c, lines - i);
    }
}

void print_line(char c, int n)
{
    for(int i = 0; i < n; ++i) {
        putchar(c);
    }
    putchar('\n');
}
