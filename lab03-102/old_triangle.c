#include <stdio.h>

int main(int argc, char *argv[])
{
   int ret = 0; //it is 0 //test it, Ask students for the value
   int n;
 
   printf("Enter number of lines in the range 1 to 10: ");
   int r = scanf("%d", &n); 
   if (r != 1) {
      ret = 10;
      printf("ERROR: Cannot read integer value from the standard input\n");
   } else if (n < 1 || n > 10) {
      ret = 101;
      printf("ERROR: Given value %d is not within the range [1, 10]\n", n);
   } else {
      putchar('\n');
      for (int i = 0; i < n; ++i) {
         for (int j = n-i; j > 0; --j) {
            putchar('*');
         }
         putchar('\n');
      }
   }
   return ret;
}