#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include <unistd.h>
#include <termios.h>

#include "prg_serial.h"

void set_raw(_Bool set);

int main(int argc, char *argv[])
{
    int ret = 0;
    const char *serial = argc > 1 ? argv[1] : "/dev/ttyACM0"; // cuaU0 nahraďte příslušným rozhraním sériového portu
    fprintf(stderr, "INFO: open serial port at %s\n", serial);
    int fd = serial_open(serial);
    if (fd != -1)
    { // read from serial port
        _Bool quit = false;
        set_raw(true);
        while (!quit)
        {
            int c = getchar();
            _Bool write_read_response = false;
            switch (c)
            {
            case 'q':
                printf("Quit the program\n");
                quit = true;
                break;
            case 's':
                printf("Send 's' - LED on\n");
                write_read_response = true;
                break;
            case 'e':
                printf("Send 'e' - LED off\n");
                write_read_response = true;
                break;
            default: // nothing to do
                if(c >= '1' && c <= '5') {
                    printf("Send '%c' - LED frequency %c\n", c, c);
                    write_read_response = true;
                }
                break;
            } // end switch c
            if (write_read_response)
            {
                int r = serial_putc(fd, c);
                if (r != -1)
                {
                    fprintf(stderr, "DEBUG: Received response '%d'\n", r);
                }
                else
                {
                    fprintf(stderr, "ERROR: Error in received responses\n");
                }
            }
        } // end while()
        set_raw(false);
        serial_close(fd);
    }
    else
    {
        fprintf(stderr, "ERROR: Cannot open device %s\n", serial);
    }
    return ret;
}

void set_raw(_Bool set)
{
    static struct termios tio, tioOld;
    tcgetattr(STDIN_FILENO, &tio);
    if (set)
    {                 // put the terminal to raw
        tioOld = tio; // backup
        cfmakeraw(&tio);
        tio.c_lflag &= ~ECHO; // assure echo is disabled
        tio.c_oflag |= OPOST; // enable output postprocessing
        tcsetattr(STDIN_FILENO, TCSANOW, &tio);
    }
    else
    { // set the previous settingsreset
        tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
    }
}