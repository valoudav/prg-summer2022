#include <stdio.h>

int sum(int a, int b);
void print_seq(int i, int n);
int sum_arr(int *begin, int *end, int acc);

int main()
{
    //print_seq(1, 10);

    int pole[10];
    int arr[] = {1,2,3,4,5};

    printf("arr[0] = %d\n", *arr);
    printf("arr[2] = %d\n", *(arr + 2));

    printf("%d\n", sum_arr(arr, arr+4, 0));

    int sum = 0;
    int *end = arr+5;
    for(int *begin = arr; begin != end; begin = begin + 1) {
        sum += *begin;
    }
    printf("%d\n", sum);


    return 0;
}

int sum_arr(int *begin, int *end, int acc)
{
    if(begin == end) {
        return *begin + acc;
    } else {
        return sum_arr(begin + 1, end, acc + *begin);
    }
}

void print_seq(int i, int n)
{
    if(i > n) {
        putchar('\n');
        return;
    }

    printf("%d ", i);
    print_seq(i+1, n);
}

int sum(int a, int b)
{
    a = a + 1;
    return a + b;
}