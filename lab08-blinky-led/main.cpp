#include "mbed.h"

#define LED_PERIOD 0.3
#define DEBOUNCE_PERIOD 0.2
#define LONG_PRESS 0.5f

const static float periods[] = {1.0, 0.5, 0.2, 0.1, 0.05};
const static int n_periods = sizeof(periods)/sizeof(float);
static int pcur = 0;

DigitalOut myled(LED1);
Ticker led_ticker;
InterruptIn button(USER_BUTTON);
Timer press_timer;

Ticker debounce_ticker;
int button_hot = 1;
void reset_button()
{
    button_hot = 1;
    debounce_ticker.detach();   
}
void debounce()
{
    button_hot = 0;
    debounce_ticker.attach(reset_button, DEBOUNCE_PERIOD);
}

void led_switch()
{
    myled = !myled;   
}

void button_press()
{
    if(button_hot) {
        debounce();
        press_timer.start();
        
        pcur++;
        pcur %= n_periods + 1;
        if(pcur == n_periods) {
            led_ticker.detach();
            myled = 1;      
        } else {
            led_ticker.attach(led_switch, periods[pcur]);
        }
    }  
}

void button_release()
{
    if(button_hot) {
        press_timer.stop();
        if(press_timer.read() >= LONG_PRESS) {
            pcur = 0;
            led_ticker.attach(led_switch, periods[pcur]);
        }
    }
}

int main() {
    
    led_ticker.attach(led_switch, periods[pcur]);
    button.fall(button_press);
    button.rise(button_release);
    
    while(1) {
    }
}

