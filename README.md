# PRG-summer2022

This repository contains codes from PRG labs for parallels

* 101 -- wednesday 11:00-12:30
* 102 -- wednesday 12:45-14:15

To download for the first time use `git clone https://gitlab.fel.cvut.cz/valoudav/prg-summer2022.git` inside the directory where you want the `prg-summer2022` directory to be located.

To update to latest version use `git pull` INSIDE the `prg-summer2020` directory:

THE CODE IS PROVIDED “AS IS” IN THE END OF THE LABORATORY EXERCISE, WITHOUT WARRANTY OF ANY KIND!
