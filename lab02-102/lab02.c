#include <stdio.h>

#define N 10

void print_seq(int i, int n);
int gcd(int a, int b);

int main()
{
    // int size = 5;
    // for(int i = 0; i < size; ++i) {
    //     for(int j = 0; j < size; ++j) {
    //         if(1 || i == j || i + j == size - 1) {
    //             printf("%2d ", i * size + j);
    //         } else {
    //             printf("   ");
    //         }
    //     }
    //     printf("\n");
    // }

    // return 0;

    // for(int i = 0; i < 5; ++i) {
    //     for(int j = 0; j < 5 - i; ++j) {
    //         putchar('*');
    //     }
    //     putchar('\n');
    // }

    // return 0;

    // char c;
    // c = getchar();
    // printf("as char: %c\n", c);
    // printf("as decimal: %d\n", c);
    // printf("as hex: 0x%x\n", c);
    // for (int j = 7; j >= 0; j--){
    //     if ((c & 1<<j) != 0 ) {
    //         printf("1");
    //     } else {
    //         printf("0");
    //     }
    // }
    // printf("\n");


    // return 0;

    print_seq(1, N);

    int i = 1;
    while (i <= N)
    {
        printf("%d ", i);
        ++i;
    }
    printf("\n");

    i = 1;
    for(;;) {
        if (i > N) {
            break;
        }
        printf("%d ", i);
        i++;
    }
    printf("\n");

    i =1;
    do {
        printf("%d ", i);
        i++;
    } while(i <= N);
    printf("\n");

    int a = 26;
    int b = 169;
    printf("gcd(%d, %d): %d\n", a, b, gcd(a,b));
   
    return 0;

}

int gcd(int a, int b)
{
    if(a < b) {
        return gcd(b, a);
    } else if(a%b == 0) {
        return b;
    } else {
        printf("%c", (&a)[100000]);
        return gcd(b, a%b);
    }
}

void print_seq(int i, int n)
{
    if(i > n) {
        putchar('\n');
        return;
    }
    printf("%d ", i);

    print_seq(i+1, n);
}